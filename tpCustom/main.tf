terraform{
  required_version=">= 0.13.4"
  required_providers{
    openstack = {
      source = "terraform-provider-openstack/openstack"
      version = "~> 1.35.0"
    }
  }
}
resource "openstack_compute_keypair_v2" "test-keypair" {
  provider = openstack
  name = "cloud"
  public_key = file("~/.ssh/cloud.key.pub")
}

resource "openstack_compute_instance_v2" "terraform_instancesv2" {
  count = 2
  name = "terraform_instancesv2-${count.index}"
  image_name = "ubuntu-20.04"
  flavor_name = "moyenne"
  key_pair = openstack_compute_keypair_v2.test-keypair.name
}
